//Array to hold all domCells
var domCells = [];
var cells = [];
var numOfdomCells = 9;
var humanGoesFirst = true;
var container = document.getElementById('container');

(function createDomCells() {
    var cell;
    for (var idx = 0; idx < numOfdomCells; idx++ ) {
        
        cell = document.createElement('div');
        cell.setAttribute('id', 'cell' + idx );
        cell.setAttribute('class', 'cell');
        
        domCells.push(cell);
        cells.push(false);
        container.appendChild(cell);
    }
})();

function cellIsOccupied(clickedCell) {
    var cellPosition = parseInt(clickedCell.id.substring(4),10);
    return ( cells[cellPosition] );    
}

function numOfOccupiedCells() {
    var num = 0;
    for(var idx = 0; idx < cells.length; idx++) {
        if (cells[idx]) { num++ };
    }
    return num;
}

function getUnOccupiedCells() {
    var unOccupiedCells = [];
    for (var idx = 0; idx < numOfdomCells; idx++) {
        if(!cells[idx]) { unOccupiedCells.push(idx); }
    }
    return unOccupiedCells;
}

function isPlayersTurn() {
    if (humanGoesFirst) {
        return ( numOfOccupiedCells() % 2 === 0 );
    }
    else {
        return ( numOfOccupiedCells() % 2 !== 0 );    
    }
}

function gameIsOver() {
    return (numOfOccupiedCells() % numOfdomCells === 0);
}

function registerPlayerMove(clickedCell) {
    clickedCell.className += ' cross';
    cells[parseInt(clickedCell.id.substring(4),10)] = true; 
}

function executeComputerMove() {
    var playThisIndex = 0;
    //playThisIndex = getUnOccupiedCells()[0];
    
    /*With this code, the computer simply places its piece on the next unoccupied cell starting from 0
    for( var idx = 0; idx < cells.length; idx++) {
        if(cells[idx] === false) {
            playThisIndex = idx;
            cells[idx] = true;
            idx = cells.length;
        }
    }*/
    
    /*With this code, the computer randomly picks an unoccupied cell to place its piece*/
    var unOccupiedCells = getUnOccupiedCells();
    var randomUnOccupiedCellIndex = Math.floor( Math.random() * unOccupiedCells.length );    
    playThisIndex = unOccupiedCells[randomUnOccupiedCellIndex];
    
    //set the cells array index to true to indicate that the cell is now occupied
    cells[playThisIndex] = true;    
    
    
    var cell = document.getElementById('cell' + playThisIndex);
    cell.className += ' nought';
}

function executePlayerMove(clickedCell) {
    registerPlayerMove(clickedCell);
    
    if(!gameIsOver()) {
        executeComputerMove();
    }
}

function handlePlayerClick() { 
    if(!cellIsOccupied(this) && isPlayersTurn()) {
        executePlayerMove(this);
    }
}

for(var i = 0; i < domCells.length; i++) { 
    domCells[i].addEventListener('click', handlePlayerClick ); 
}








































